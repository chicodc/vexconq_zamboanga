var connection = require('./conn.js');
var util = require('util');

function addcount(){
	connection.query('SELECT * FROM client_count_perday WHERE DATE(date_time) = CURDATE()', function(err, result){
		if(err){
			console.log('ERROR on que.js addcount : %s', err);
		}else{
			if(result.length){
				var newCount = result[0].client_count + 1;
				connection.query('UPDATE client_count_perday SET client_count ='+newCount+' WHERE DATE(date_time) = CURDATE()',function(error, result){
					if(error) console.log('ERROR on que.js addcount : %s', error);
					console.log('UPDATED ' + result.affectedRows);
				});
			}else{
				connection.query('SELECT * FROM pending WHERE DATE(date_of_transaction) = CURDATE()', function(error, results){
					if(error){
						console.log('ERROR on que.js addcount : %s', err)
					}else{
						if(results.length){
							console.log(results.length);
							var newCount = results.length;
							connection.query('INSERT INTO client_count_perday(client_count) VALUES ('+newCount+')', function(errors, result){
								if (errors) console.log('ERROR on que.js addcount : %s', errors);
	  							console.log('INSERTED ' + result.insertId);
							});
						}
					}
				});
			}
		}
	});
};



module.exports.getallpending = function(req, res){
	var pending = {};
	var pending_count = {'count' : 0};
	var all = req.params.all;
	var add_query ='';
	var add_query_count = '';
	if(all != 'getall'){
		add_query = 'AND a.flag IS NULL';
		add_query_count = 'AND flag IS NULL';
	}

	var query = "SELECT \
					(select count(*) from pending where date(date_of_transaction) = CURDATE() " + add_query_count + ") AS count, \
					a.numberque, a.quecard, a.salutation, a.firstname, a.lastname, a.status, s.service, s.icon, a.ar, a.prepared, a.flag, a.date_of_transaction, \
					DATE_FORMAT(a.date_of_transaction,'%h:%i:%s %p') TIMEONLY, a.alias, b.firstname as preparedbyname, \
					b.lastname as preparedbylast, c.firstname as flagname, c.lastname as flaglast \
				FROM pending a \
				LEFT JOIN users b on a.prepared = b.id \
				LEFT JOIN users c on a.flag = c.id \
				LEFT JOIN status s on a.status = s.id \
				WHERE date(a.date_of_transaction) = CURDATE() " + add_query + " \
				ORDER BY a.numberque ASC";

	connection.query(query , function(err_pending, results_pending){
		if(err_pending) console.log('ERROR on getallpending : %s', err_pending);
		if(results_pending){
			pending = results_pending; pending_count = results_pending[0];
		}
		if(!results_pending.length){
			pending_count = {'count' : 0};
		}
		var response = {"pending" : pending, 'pending_count' : pending_count };
		res.send(response);
	});
};


module.exports.getallskipped = function(req,res){
	var skipped = {};
	var skipped_count = {'count' : 0};
	var sql = "SELECT (select count(*) from skipped where date(date_of_transaction) = CURDATE()) AS count, u.firstname as agent_f, \
						u.lastname as agent_l, s.numberque, s.quecard, s.salutation, s.firstname, s.lastname, s.status, s.agent, s.ar, s.prepared, \
						s.alias, stat.service, stat.icon \
				FROM skipped s LEFT JOIN users u \
				ON u.id = s.flag \
				LEFT JOIN status stat on s.status = stat.id \
				AND date(date_of_transaction) = CURDATE() \
				ORDER BY numberque ASC";
	connection.query( sql, function(err_skipped, results_skipped){
		if(err_skipped){
			console.log('ERROR on getallskipped : %s', err_skipped);
		}else{
			if(results_skipped){skipped = results_skipped; skipped_count = results_skipped[0];}
			if(!results_skipped.length){skipped_count = {'count' : 0};}
			var response = {"skipped" : skipped, "skipped_count" : skipped_count };
			res.send(response);
		}
	});
};


module.exports.getallserved = function(req, res){
	var served = {};
	var served_count = {'count' : 0};
	var query = 'SELECT (select count(*) from served where date(date_of_transaction) = CURDATE()) AS count, ser.user_id, ser.numberque, \
							ser.salutation, ser.quecard, ser.firstname, ser.lastname, ser.status, ser.agent, DATE_FORMAT(ser.time_served,"%h:%i:%s %p") TIMEONLY, \
							DATE_FORMAT(ser.date_of_transaction,"%h:%i:%s %p") ARRIVAL, stat.service, stat.icon \
					FROM served ser\
					LEFT JOIN status stat on ser.status = stat.id \
					WHERE date(date_of_transaction) = CURDATE() ORDER BY numberque ASC';
	connection.query( query, function(err_reserved, results_served){
		if(err_reserved){
			console.log('ERROR on que.js getallserved : %s', err_reserved);
		}else{
			if(results_served){served = results_served; served_count = results_served[0];}
			if(!results_served.length){served_count = {'count' : 0};}
			var response = {"served" : served, "served_count" : served_count };
			res.send(response);
		}
	});
};

module.exports.getallservedarchive = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	var whereClause = "";
	if(data.dateFrom == undefined){
		data.dateFrom = "";
	}
	if(data.dateTo == undefined){
		data.dateTo = "";
	}
	if(data.dateFrom != "" && data.dateTo != ""){
		whereClause = "WHERE DATE(s.date_of_transaction) BETWEEN ? AND ?";
	}
	query = "SELECT \
				TIMEDIFF(s.time_called, s.date_of_transaction ) as waitingtime, \
			    TIMEDIFF(s.time_served, s.time_called ) as servingtime,  \
			    c.service as status,\
				s.numberque, s.quecard, \
				s.firstname, s.lastname, s.date_of_transaction, s.time_called, s.time_served, s.agent, s.alias\
			FROM (SELECT * FROM served_archive UNION ALL SELECT * FROM served) s\
			LEFT JOIN status c\
			ON s.status = c.id";
	connection.query(query + ' ' + whereClause, [data.dateFrom, data.dateTo], function(err, results){
		if(err){
			console.log("Error inserting : %s ",err );
     		var response = {success : 'ERROR' , name : req.session.name};
     		res.send(response);
    	 }else{
     		var response = {success : results , name : req.session.name};
     		res.send(response);
    	 }
	});
};


module.exports.getperservice = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	var query = '';
	var prepared = '';
	var hasPrepared = false;


	switch(data.charttype){
		case 'percentage' :
				query = "SELECT COUNT(s.status), \
						    c.service as category, \
							FORMAT((COUNT(s.status) / (SELECT COUNT(*) FROM (SELECT * FROM served_archive UNION ALL SELECT * FROM served ) s WHERE DATE(s.date_of_transaction) BETWEEN '"+ data.from +"' AND '"+ data.to +"')) * 100, 2) AS 'PERCENT' \
						FROM \
							status c \
						LEFT JOIN \
							(SELECT * FROM served_archive UNION ALL SELECT * FROM served ) s \
						ON s.status = c.id \
						AND DATE(s.date_of_transaction) BETWEEN '"+ data.from +"' AND '"+ data.to +"' \
						GROUP BY s.status";

						break;

		case 'averageServiceTime' :
					prepared = "SET @sql = NULL; \
							SELECT \
							  GROUP_CONCAT(DISTINCT \
							    CONCAT('FORMAT(IFNULL(AVG(CASE WHEN status = ''', id ,''' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter )) / 60 END), 0), 2 ) as ', REPLACE(service, ' ', '_')) \
							  ) INTO @sql \
							FROM status; \
							SET @sql = CONCAT( \
								'SELECT u.dte as category, ', @sql, ' FROM (SELECT ''"+ data.from +"'' + INTERVAL a + b DAY dte \
										FROM \
											(SELECT 0 a UNION SELECT 1 a UNION SELECT 2 UNION SELECT 3 \
												UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 \
												UNION SELECT 8 UNION SELECT 9 ) d, \
											(SELECT 0 b UNION SELECT 10 UNION SELECT 20 \
												UNION SELECT 30 UNION SELECT 40) m \
										WHERE ''"+ data.from +"'' + INTERVAL a + b DAY  <=  ''"+ data.to +"'' \
										ORDER BY a + b \
									) u \
								LEFT JOIN (SELECT * FROM served_archive UNION ALL SELECT * FROM served) s \
									ON DATE(s.date_of_transaction)  = u.dte \
									GROUP BY u.dte' \
							); \
							PREPARE stmt FROM @sql; ";
					connection.query(prepared, function(err, result){
						if(err){
							console.log('ERROR on prepared statement 2 : %s', err);
						}
					});
					query = "EXECUTE stmt;";
					hasPrepared = true;

					break;

		case 'timePerAgent' :
				prepared = "SET @sql = NULL; \
							SELECT \
							  GROUP_CONCAT(DISTINCT \
							    CONCAT('CEIL(IFNULL(AVG(CASE WHEN status = ''', id ,''' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter )) / 60 END), 0) ) AS ', REPLACE(service, ' ', '_')) \
							  ) INTO @sql \
							FROM status; \
							SET @sql = CONCAT( 'SELECT \
													u.firstname as category, \
													', @sql, ' \
												FROM users u \
												 LEFT JOIN (SELECT * FROM served_archive UNION ALL SELECT * FROM served) s \
												ON s.user_id = u.id \
												AND DATE(s.date_of_transaction) BETWEEN ''"+ data.from +"'' AND ''"+ data.to +"'' \
												GROUP BY u.id' \
							); \
							PREPARE stmt FROM @sql; ";
				connection.query(prepared, function(err, result){
					if(err){
						console.log('ERROR on prepared statement 3 : %s', err);
					}
				});
				query = "EXECUTE stmt;";
				hasPrepared = true;

				break;

		case 'clientPerDate' :
				query = "SELECT \
							u.dte as category,\
							COUNT(s.status) as count \
						FROM \
									(\
									SELECT '"+ data.from +"' + INTERVAL a + b DAY dte\
									FROM\
									 (SELECT 0 a UNION SELECT 1 a UNION SELECT 2 UNION SELECT 3\
									    UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7\
									    UNION SELECT 8 UNION SELECT 9 ) d,\
									 (SELECT 0 b UNION SELECT 10 UNION SELECT 20 \
									    UNION SELECT 30 UNION SELECT 40) m\
									WHERE '"+ data.from +"' + INTERVAL a + b DAY  <=  '"+ data.to +"'\
									ORDER BY a + b\
									) u\
						LEFT JOIN (SELECT * FROM served_archive UNION ALL SELECT * FROM served) s \
						ON DATE(s.date_of_transaction)  = u.dte\
						GROUP BY u.dte";

						break;

		case 'clientPerHour' :
				prepared = "SET @sql = NULL; \
							SELECT \
							  GROUP_CONCAT(DISTINCT \
							    CONCAT('IFNULL(COUNT(CASE WHEN status = ''', id ,''' THEN s.status END), 0) AS ', REPLACE(service, ' ', '_')) \
							  ) INTO @sql \
							FROM status; \
							SET @sql = CONCAT('SELECT \
													TIME_FORMAT(m.merge_date, ''%h%p'') as category, \
													', @sql, ' \
												FROM ( \
												           SELECT ''10:00:00'' AS merge_date \
												           UNION SELECT ''11:00:00'' AS merge_date \
												           UNION SELECT ''12:00:00'' AS merge_date \
												           UNION SELECT ''13:00:00'' AS merge_date \
												           UNION SELECT ''14:00:00'' AS merge_date \
												           UNION SELECT ''15:00:00'' AS merge_date \
												           UNION SELECT ''16:00:00'' AS merge_date \
												           UNION SELECT ''17:00:00'' AS merge_date \
												           UNION SELECT ''18:00:00'' AS merge_date \
												           UNION SELECT ''19:00:00'' AS merge_date \
												           UNION SELECT ''20:00:00'' AS merge_date \
												           UNION SELECT ''21:00:00'' AS merge_date \
												           UNION SELECT ''22:00:00'' AS merge_date \
												          ) AS m \
												LEFT JOIN (SELECT * FROM served_archive UNION ALL SELECT * FROM served) s 	\
												ON HOUR(s.date_of_transaction)  = HOUR(m.merge_date) \
												AND Date(date_of_transaction) BETWEEN ''"+ data.from +"'' AND ''"+ data.to +"'' \
												GROUP BY m.merge_date');\
							PREPARE stmt FROM @sql;";
				connection.query(prepared, function(err, result){
					if(err){
						console.log('ERROR on prepared statement 2 : %s', err);
					}
				});
				query = "EXECUTE stmt;";
				hasPrepared = true;

				break;

		case 'clientPerWeekDay' :
				prepared = "SET @sql = NULL; \
							SELECT \
							  GROUP_CONCAT(DISTINCT \
							    CONCAT('IFNULL(COUNT(CASE WHEN status = ''', id ,''' THEN s.status END), 0) AS ', REPLACE(service, ' ', '_')) \
							  ) INTO @sql \
							FROM status; \
							SET @sql = CONCAT('SELECT \
												m.weekday as category, \
												', @sql, ' \
											FROM ( \
											           SELECT ''SUNDAY'' AS weekday \
											           UNION SELECT ''MONDAY'' AS weekday \
											           UNION SELECT ''TUESDAY'' AS weekday \
											           UNION SELECT ''WEDNESDAY'' AS weekday \
											           UNION SELECT ''THURSDAY'' AS weekday \
											           UNION SELECT ''FRIDAY'' AS weekday \
											           UNION SELECT ''SATURDAY'' AS weekday \
											          ) AS m \
											LEFT JOIN (SELECT * FROM served_archive UNION ALL SELECT * FROM served) s 	\
											ON DAYNAME(s.date_of_transaction)  = m.weekday \
											AND Date(date_of_transaction) BETWEEN ''"+ data.from +"'' AND ''"+ data.to +"'' \
											GROUP BY m.weekday\
											ORDER BY DAYOFWEEK(s.date_of_transaction)');\
							PREPARE stmt FROM @sql;"
				connection.query(prepared, function(err, result){
					if(err){
						console.log('ERROR on prepared statement 2 : %s', err);
					}
				});
				query = "EXECUTE stmt;";
				hasPrepared = true;

				break;

		case 'monthlyPercentage' :

				query = "SELECT\
							c.service as status,\
							YEAR(s.date_of_transaction) AS year,\
							Count(case when month(date_of_transaction) = 1 then s.status end) As Jan,\
							Count(case when month(date_of_transaction) = 2 then s.status end) As Feb,\
							Count(case when month(date_of_transaction) = 3 then s.status end) As Mar,\
							COUNT(CASE WHEN MONTH(date_of_transaction) = 4 THEN s.status END) AS Apr,\
							COUNT(CASE WHEN MONTH(date_of_transaction) = 5 THEN s.status END) AS May,\
							COUNT(CASE WHEN MONTH(date_of_transaction) = 6 THEN s.status END) AS Jun,\
							COUNT(CASE WHEN MONTH(date_of_transaction) = 7 THEN s.status END) AS Jul,\
							COUNT(CASE WHEN MONTH(date_of_transaction) = 8 THEN s.status END) AS Aug,\
							COUNT(CASE WHEN MONTH(date_of_transaction) = 9 THEN s.status END) AS Sep,\
							COUNT(CASE WHEN MONTH(date_of_transaction) = 10 THEN s.status END) AS Oct,\
							COUNT(CASE WHEN MONTH(date_of_transaction) = 11 THEN s.status END) AS Nov,\
						 	COUNT(CASE WHEN MONTH(date_of_transaction) = 12 THEN s.status END) AS December\
						 FROM (SELECT * FROM served_archive UNION ALL SELECT * FROM served) s\
						 LEFT JOIN status c ON s.status = c.id \
						 WHERE YEAR(s.date_of_transaction) = '" + data.yearmonth + "'\
						 GROUP BY s.status\
				         ORDER BY s.status";

						break;

		case 'monthlyAverage' :
				prepared = "SET @sql = NULL; \
							SELECT \
							  GROUP_CONCAT(DISTINCT \
							    CONCAT('FORMAT(IFNULL(AVG(CASE WHEN status = ''', id ,''' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter )) / 60 END), 0), 2 ) AS ', REPLACE(service, ' ', '_')) \
							  ) INTO @sql \
							FROM status; \
							SET @sql = CONCAT('SELECT DATE_FORMAT(m.merge_date, ''%M'') as category, \
												', @sql ,' \
											 FROM ( \
										           SELECT ''2013-01-01'' AS merge_date \
										           UNION SELECT ''2013-02-01'' AS merge_date \
										           UNION SELECT ''2013-03-01'' AS merge_date \
										           UNION SELECT ''2013-04-01'' AS merge_date \
										           UNION SELECT ''2013-05-01'' AS merge_date \
										           UNION SELECT ''2013-06-01'' AS merge_date \
										           UNION SELECT ''2013-07-01'' AS merge_date \
										           UNION SELECT ''2013-08-01'' AS merge_date \
										           UNION SELECT ''2013-09-01'' AS merge_date \
										           UNION SELECT ''2013-10-01'' AS merge_date \
										           UNION SELECT ''2013-11-01'' AS merge_date \
										           UNION SELECT ''2013-12-01'' AS merge_date \
										          ) AS m \
											LEFT JOIN \
											(SELECT * FROM served_archive UNION ALL SELECT * FROM served) s \
											ON MONTH(m.merge_date) = MONTH(s.date_of_transaction) \
											AND YEAR(s.date_of_transaction) = ''" + data.yearmonth + "'' \
											GROUP BY MONTH(m.merge_date)'); \
							PREPARE stmt FROM @sql";
				connection.query(prepared, function(err, result){
					if(err){
						console.log('ERROR on prepared statement 2 : %s', err);
					}
				});
				query = "EXECUTE STMT;";
				hasPrepared	= true;

				break;

			default : '';
	}
	connection.query(query, function(err, results){
		if(err){
			console.log("Error reporting : %s ",err );
     		var response = {success : 'ERROR' , name : req.session.name};
     		res.send(response);
    	 }else{
    	 	if(results.length){
     			var response = {success : results , name : req.session.name};
     			res.send(response);
     			if(hasPrepared){ connection.query("DEALLOCATE PREPARE stmt;");}
     		}else{
     			var response = {success : 'ERROR', message : 'NO DATA FOUND'};
     			res.send(response);
     			if(hasPrepared){ connection.query("DEALLOCATE PREPARE stmt;");}
     		}
    	 }
	});
};


module.exports.getSummary = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	var query = '';

	switch(data.charttype){
		case 'percentage' :
				//important for query : count(distinct date(s.date_of_transaction)
				query = "SELECT c.service as category, \
								COUNT(s.status) as client_count, \
								FLOOR(COUNT(s.status) / (DATEDIFF( '" + data.to + "', '" + data.from + "' ) + 1)) as client_avg, \
								IFNULL(MAX(TIMEDIFF(s.time_on_counter, s.date_of_transaction)), '00:00:00') as max_wait, \
								IFNULL(SEC_TO_TIME(CEIL(SUM(TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction))) / count(distinct(s.date_of_transaction)))), '00:00:00') as avg_wait, \
								IFNULL(SEC_TO_TIME(CEIL(SUM(TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter))) / count(distinct(s.date_of_transaction)))), '00:00:00') as avg_serve \
							FROM \
								status c \
							LEFT JOIN \
								( SELECT * FROM served_archive UNION ALL SELECT * FROM served ) s \
							ON s.status = c.id\
							AND date(s.date_of_transaction) BETWEEN ? AND ? \
							GROUP BY c.id";
		break;


		case 'timePerAgent' :
				query = "SELECT \
							u.id as filter, \
							CONCAT(u.firstname, ' ', u.lastname) AS category,\
							IFNULL(c.service, 'NONE') as status,\
							COUNT(s.status) as count,\
							IFNULL(MAX(TIMEDIFF(s.time_on_counter, s.date_of_transaction)), '00:00:00') as max_wait,\
							IFNULL(SEC_TO_TIME(CEIL(SUM(TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction))) / COUNT(s.status))), '00:00:00') as avg_wait,\
							IFNULL(SEC_TO_TIME(CEIL(SUM(TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter))) / COUNT(s.status))), '00:00:00') as avg_serve\
						FROM users u\
						LEFT JOIN (SELECT * FROM served_archive UNION ALL SELECT * FROM served) s \
						ON s.user_id  = u.id\
						AND DATE(s.date_of_transaction) BETWEEN  '" + data.from + "' AND '" + data.to + "'\
						LEFT JOIN status c on s.status = c.id \
						GROUP BY u.id, s.status";
		break;


		case 'clientPerDate' :
				query = "SELECT \
								u.dte as category,\
								u.dte as filter,\
								IFNULL(c.service, 'NONE') as status,\
								COUNT(s.status) as count,\
								IFNULL(MAX(TIMEDIFF(s.time_on_counter, s.date_of_transaction)), '00:00:00') as max_wait,\
								IFNULL(SEC_TO_TIME(CEIL(SUM(TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction))) / COUNT(s.status))), '00:00:00') as avg_wait,\
								IFNULL(SEC_TO_TIME(CEIL(SUM(TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter))) / COUNT(s.status))), '00:00:00') as avg_serve\
						FROM \
								(\
									SELECT '" + data.from + "' + INTERVAL a + b DAY dte\
									FROM\
									(SELECT 0 a UNION SELECT 1 a UNION SELECT 2 UNION SELECT 3\
										UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7\
										UNION SELECT 8 UNION SELECT 9 ) d,\
									(SELECT 0 b UNION SELECT 10 UNION SELECT 20 \
										UNION SELECT 30 UNION SELECT 40) m\
									WHERE '" + data.from + "' + INTERVAL a + b DAY  <=  '" + data.to + "'\
										ORDER BY a + b\
								) u\
						LEFT JOIN (SELECT * FROM served_archive UNION ALL SELECT * FROM served) s \
						ON DATE(s.date_of_transaction)  = u.dte\
						AND DATE(s.date_of_transaction) BETWEEN  '" + data.from + "' AND '" + data.to + "'\
						LEFT JOIN status c on s.status = c.id \
						GROUP BY u.dte, s.status";
		break;

		case 'clientPerWeekDay' :
				query = "SELECT \
							c.service as category, \
							COUNT(CASE WHEN DAYNAME(s.date_of_transaction) = 'SUNDAY' THEN s.status END)  as SUNDAY_COUNT,\
							IFNULL(SEC_TO_TIME(CEIL( SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'SUNDAY' THEN  TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'SUNDAY' THEN date(s.date_of_transaction) END))),'00:00:00') as SUNDAY_AVGWAIT,\
							IFNULL(SEC_TO_TIME(CEIL(SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'SUNDAY' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'SUNDAY' THEN date(s.date_of_transaction) END))),'00:00:00') as SUNDAY_AVGSERVE,\
							COUNT(CASE WHEN DAYNAME(s.date_of_transaction) = 'MONDAY' THEN s.status END)  as MONDAY_COUNT,\
							IFNULL(SEC_TO_TIME(CEIL( SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'MONDAY' THEN  TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'MONDAY' THEN date(s.date_of_transaction) END))), '00:00:00') as MONDAY_AVGWAIT,\
							IFNULL(SEC_TO_TIME(CEIL(SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'MONDAY' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'MONDAY' THEN date(s.date_of_transaction) END))),'00:00:00') as MONDAY_AVGSERVE,\
							COUNT(CASE WHEN DAYNAME(s.date_of_transaction) = 'TUESDAY' THEN s.status END)  as TUESDAY_COUNT,\
							IFNULL(SEC_TO_TIME(CEIL( SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'TUESDAY' THEN  TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'TUESDAY' THEN date(s.date_of_transaction) END))),'00:00:00') as TUESDAY_AVGWAIT,\
							IFNULL(SEC_TO_TIME(CEIL(SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'TUESDAY' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'TUESDAY' THEN date(s.date_of_transaction) END))), '00:00:00') as TUESDAY_AVGSERVE,\
							COUNT(CASE WHEN DAYNAME(s.date_of_transaction) = 'WEDNESDAY' THEN s.status END)  as WEDNESDAY_COUNT,\
							IFNULL(SEC_TO_TIME(CEIL( SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'WEDNESDAY' THEN  TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'WEDNESDAY' THEN date(s.date_of_transaction) END))),'00:00:00') as WEDNESDAY_AVGWAIT,\
							IFNULL(SEC_TO_TIME(CEIL(SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'WEDNESDAY' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'WEDNESDAY' THEN date(s.date_of_transaction) END))), '00:00:00') as WEDNESDAY_AVGSERVE,\
							COUNT(CASE WHEN DAYNAME(s.date_of_transaction) = 'THURSDAY' THEN s.status END)  as THURSDAY_COUNT,\
							IFNULL(SEC_TO_TIME(CEIL( SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'THURSDAY' THEN  TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'THURSDAY' THEN date(s.date_of_transaction) END))) , '00:00:00') as THURSDAY_AVGWAIT,\
							IFNULL(SEC_TO_TIME(CEIL(SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'THURSDAY' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'THURSDAY' THEN date(s.date_of_transaction) END))), '00:00:00') as THURSDAY_AVGSERVE,\
							COUNT(CASE WHEN DAYNAME(s.date_of_transaction) = 'FRIDAY' THEN s.status END)  as FRIDAY_COUNT,\
							IFNULL(SEC_TO_TIME(CEIL( SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'FRIDAY' THEN  TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'FRIDAY' THEN date(s.date_of_transaction) END))), '00:00:00') as FRIDAY_AVGWAIT,\
							IFNULL(SEC_TO_TIME(CEIL(SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'FRIDAY' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'FRIDAY' THEN date(s.date_of_transaction) END))), '00:00:00') as FRIDAY_AVGSERVE,\
							COUNT(CASE WHEN DAYNAME(s.date_of_transaction) = 'SATURDAY' THEN s.status END)  as SATURDAY_COUNT,\
							IFNULL( SEC_TO_TIME(CEIL(SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'SATURDAY' THEN  TIME_TO_SEC(TIMEDIFF(s.time_on_counter, s.date_of_transaction)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'SATURDAY' THEN date(s.date_of_transaction) END))), '00:00:00') as SATURDAY_AVGWAIT,\
							IFNULL(SEC_TO_TIME(CEIL(SUM(CASE WHEN DAYNAME(s.date_of_transaction) = 'SATURDAY' THEN TIME_TO_SEC(TIMEDIFF(s.time_served, s.time_on_counter)) END) \
								/ count(CASE WHEN DAYNAME(s.date_of_transaction) = 'SATURDAY' THEN date(s.date_of_transaction) END))), '00:00:00') as SATURDAY_AVGSERVE\
						 FROM  \
						 	status c \
						LEFT JOIN \
							( SELECT * FROM served_archive UNION ALL SELECT * FROM served ) s \
						ON s.status = c.id\
						AND date(s.date_of_transaction) BETWEEN ? AND ? \
						GROUP BY c.id";
		break;

		default : '';
	}

	connection.query(query, [data.from, data.to], function(err, result){
		if(err){
			console.log('ERROR on getSummary : %s', err);
		}else{
			if(result.length){
     			var response = {success : result , name : req.session.name};
     			res.send(response);
     		}else{
     			var response = {success : 'ERROR', message : 'NO DATA FOUND'};
     			res.send(response);
     		}
		}
	});
};

module.exports.getcurrent = function(req, res){
	var current;

	connection.query("SELECT * FROM pending \
						WHERE flag = '"+ req.session.userid + "' AND status = '"+ req.session.transaction +"' \
						AND date(date_of_transaction) = CURDATE()", function(err_results, results_current){
		if(err_results){
			console.log('ERROR on que.js getcurrent : %s', err_results);
		}else{
			if(results_current.length){
				current = results_current;
				req.session.current_table = "pending";
				req.session.current = results_current[0].id;
				console.log(results_current[0].id);
			}
			else if(!results_current.length){current = 'NONE';}

			var response = {"current" : current};
			res.send(response);
		}
	});
};


module.exports.getcurrentskipped = function(req, res){
	var current_skipped;

	connection.query("SELECT id, numberque, quecard, salutation, firstname, lastname, status, agent, ar, prepared, time_called FROM skipped WHERE flag = '"+ req.session.userid + "' AND status = '"+ req.session.transaction +"' AND date(date_of_transaction) = CURDATE()", function(err_skipped, current_skipped_result){
		if(err_skipped){
			console.log('ERROR on que.js getcurrentskipped : %s', err_skipped);
		}else{
			if(current_skipped_result.length){
					current_skipped = current_skipped_result;
					console.log(current_skipped_result);
				 	req.session.current_table = "skipped";
				 	req.session.current = current_skipped_result[0].id;
				 	console.log(req.session.current +' this');
			}
			else if(!current_skipped_result.length){
					current_skipped ={'numberque' : 0};
			}

			var response = { "current_skipped" : current_skipped };
			res.send(response);
		}
	});
};

module.exports.nextclient = function(req, res){
	connection.query('SELECT * FROM pending WHERE flag =' + req.session.userid, function(check_error, check_result){
		if(check_error){
			console.log('ERROR on que.js nextclient : %s', check_error);
		}else{
			if(!check_result.length){
				connection.query("SELECT id, numberque, firstname, lastname, alias FROM pending WHERE flag IS NULL AND status='"+req.session.transaction+"' AND DATE(date_of_transaction)= CURDATE() LIMIT 1", function(err, result){
					if(result.length){
						connection.query("UPDATE queserve SET client_name = ?, numberque = ? WHERE agent_id = ?", [result[0].alias, result[0].numberque, req.session.userid] , function(errs, rows){
							if(errs){
								console.log('ERROR on que.js nextclient : %s ', errs);
							}else{
								console.log('UPDATE ' + rows.affectedRows + 'on queserve table');
							}
						});
						connection.query('UPDATE pending SET flag="'+ req.session.userid +'", time_service = CURRENT_TIMESTAMP() WHERE id='+ result[0].id +' ', function(error, results){
							if(error){
								console.log('ERROR on que.js nextclient : %s', error);
						     	var response = {success : 'ERROR' , name : req.session.name};
						     	res.send(response);
						     }else{
						     	var response = {success : 'Update successful' , name : req.session.name};
						     	res.send(response);
						     }
						});
					}else{
						var response = {success : 'NONE' , name : req.session.name};
						res.send(response);
					}
				});
			}else{
				var response = {success : 'ACTIVE' , name : req.session.name, message : 'You are currently serving another client. Please finish transaction first.'};
				res.send(response);
			}
		}
	});
};


module.exports.serveclient = function(req, res){
	console.log("--------params");
	console.log(req.query);
	var alias = req.query.alias;
    var jumpnumber = req.params.jumpnumber;
    var table;
    var add_query = '';
    if(jumpnumber != 0){
    	table = 'skipped';
		add_query = '';
    }else{
    	table = 'pending';
    	add_query = "AND status='"+req.session.transaction+"'";
    }
	console.log("-----here");
    console.log(table + " " + jumpnumber);

	connection.query("SELECT * FROM "+ table + " WHERE flag = '"+req.session.userid+"'  "+add_query+" AND DATE(date_of_transaction)= CURDATE() LIMIT 1", function(err, result){
		if(err){
			console.log('ERROR on que.js serveclient : %s', err);
		}else{
			if(result.length){
				var RecordDetailParsedFromForm = {
			        user_id    	: result[0].flag,
			        numberque 	: result[0].numberque,
			        quecard		: result[0].quecard,
			        salutation 	: result[0].salutation,
			        firstname 	: result[0].firstname,
			        lastname 	: result[0].lastname,
			        status 		: result[0].status,
			        agent 		: req.session.name,
			        date_of_transaction : result[0].date_of_transaction,
			        time_called : result[0].time_called,
			        time_on_counter : result[0].time_service,
			        ar          : result[0].ar,
					alias		: alias
			    };
			    if(result[0].status == "3" && result[0].prepared == 0){
			    	var response = {success : 'UNPREPARED' , name : req.session.name};
					res.send(response);
			    }else{
			    	connection.query("UPDATE queserve SET client_name = NULL, status = NULL, numberque = NULL WHERE counter_number = '"+req.session.counter+"'", function(error, results){
			    		if(error){console.log('ERROR on que.js on serveclient : %s', error);}else{
			    			console.log('UPDATED '+ results +' queserve on que.js serveclient');
			    		}
			    	});
					connection.query("UPDATE served SET alias = '"+alias+"' WHERE numberque = '"+ result[0].numberque+"'", function(error, results){
			    		if(error){console.log('ERROR on que.js on serveclient : %s', error);}else{
			    			console.log('UPDATED '+ results +' served on que.js serveclient');
			    		}
			    	});
			    	connection.query("UPDATE quedata SET banner_name = NULL, banner_num = NULL WHERE id=1", function(error, results){
			    		if(error){console.log('ERROR on que.js on serveclient : %s', error);}else{
			    			console.log('UPDATED '+ results +' queserve on que.js serveclient');
			    		}
			    	});
					connection.query("INSERT INTO served set ?  ",[RecordDetailParsedFromForm], function(errors, rows)
				    {
				        if (errors){ console.log("Error inserting : %s ",errors );}else{
				        	connection.query("DELETE FROM "+ table +"  WHERE flag = '"+req.session.userid+"' "+add_query+" AND DATE(date_of_transaction)= CURDATE() LIMIT 1", function(error, results){
					        	if(error){
					        		console.log('ERROR on que.js on serveclient : %s', error);
							     	var response = {success : 'ERROR' , name : req.session.name};
							     	res.send(response);
							     }else{
							     	var response = {success : 'Update successful' , name : req.session.name};
							     	res.send(response);
							     }
				        	});
				    	}
				    });
				}
			}

		}
	});
};



module.exports.jumpclient = function(req, res){
	var id = req.session.userid;
	var data = JSON.parse(JSON.stringify(req.body));
	connection.query('SELECT * FROM pending WHERE flag =' + req.session.userid, function(check_error, check_result){
		if(!check_result.length){
		    connection.query('SET SQL_SAFE_UPDATES = 0', function(error, result){
		        if(error){
		            console.log('ERROR on set safe off : %s', error);
		        }else{
		            console.log('unsafe');
		        }
		    });
			connection.query("UPDATE skipped SET flag = ?, time_service = CURRENT_TIMESTAMP() WHERE flag IS NULL AND numberque = ? AND status = ?",[id, data.number, req.session.transaction], function(err, result){
				if(err){
					console.log('ERROR on skipped UPDATE : %s ', err);
		     		var response = {success : 'ERROR' , name : req.session.name};
		     		res.send(response);
		    	}else{
		    	 	connection.query('SELECT * FROM skipped WHERE numberque = ? AND flag = ?', [data.number, id],function(skipped_error, skipped_result){
		    	 		if(skipped_error){console.log('ERROR on que.js jumpclient : %s', skipped_error);}else{
			    	 		if(skipped_result.length){
								connection.query("UPDATE queserve SET client_name = ? , numberque = ? WHERE agent_id = ?", [skipped_result[0].firstname + " " + skipped_result[0].lastname, skipped_result[0].numberque, req.session.userid] , function(errs, rows){
									if(errs){console.log('ERROR on que.js jumpclient : %s',errs);}else{
							     		var response = {success : 'Update successful' , name : req.session.name};
							     		res.send(response);
									}
								});
			    	 		}
		    	 		}
					});
		    	}
			});
		}else{
			var response = {success : 'ACTIVE' , name : req.session.name, message : 'You are currently serving another client. Please finish transaction first.'};
			res.send(response);
		}
	});
};

module.exports.skipclient = function(req, res){
	connection.query("SELECT * FROM pending WHERE flag = '"+req.session.userid+"' AND status='"+req.session.transaction+"' AND DATE(date_of_transaction)= CURDATE() LIMIT 1", function(err, result){
		if(err){console.log('ERROR on que.js skipclient : %s', err);}else{
			if(result.length){
				var RecordDetailParsedFromForm = {
			        numberque 	: result[0].numberque,
			        quecard     : result[0].quecard,
			        salutation 	: result[0].salutation,
			        firstname 	: result[0].firstname,
			        lastname 	: result[0].lastname,
			        status 		: result[0].status,
			        agent 		: req.session.name,
			        ar 			: result[0].ar,
			        prepared 	: result[0].prepared,
			        alias 		: result[0].alias,
			        date_of_transaction : result[0].date_of_transaction,
			        time_service : result[0].time_service
			    };
				connection.query("INSERT INTO skipped set ?  ",[RecordDetailParsedFromForm], function(errors, rows)
			    {
			        if (err){ console.log("Error inserting : %s ",err );}else{
				        connection.query("UPDATE queserve SET client_name = NULL, status = NULL, numberque = NULL WHERE agent_id = '"+req.session.userid+"'", function(updateQueserveError, updateQueserveResults){
				        	if(updateQueserveError){ console.log('ERROR on que.js skipclient : %s', updateQueserveError); }
				        });
				        connection.query("UPDATE quedata SET banner_name = NULL, banner_num = NULL WHERE id=1", function(updateQuedataError, updateQuedataResults){
				        	if(updateQuedataError){ console.log('ERROR on que.js skipclient : %s', updateQuedataError); }
				        });
				        connection.query("DELETE FROM pending WHERE flag = '"+req.session.userid+"' AND status='"+req.session.transaction+"' AND DATE(date_of_transaction)= CURDATE() LIMIT 1", function(error, results){
				        	if(error){
				        		console.log('ERROR on que.js skipclient : %s', error);
						     	var response = {success : 'ERROR' , name : req.session.name};
						     	res.send(response);
						     }else{
						     	var response = {success : 'Update successful' , name : req.session.name};
						     	res.send(response);
						     }
				        });
			    	}
			    });

			}else{
				var response = {success : 'SKIPPED' , name : req.session.name};
				res.send(response);
			}
		}
	});
};

module.exports.callclient = function(req, res){
	var counter_number = req.session.counter;
	var current_table = req.session.current_table;
	var id = req.session.current;
	if(counter_number != undefined){
		connection.query("SELECT * FROM "+ current_table +" WHERE id = ?", [id], function(error, result){
			if(error){
				console.log('ERROR on que.js callclient : %s', error);
			}else{
				if(result[0]){
					var RecordDetailParsedFromForm = {
				        client_name 		: result[0].alias,
				        quecard				: result[0].quecard,
				        status 				: result[0].status,
				        date_of_transaction : result[0].date_of_transaction
				    };
				    var prefix = {
				    	1 : "APPOINTMENT",
							2 : "REPAIR/CHECKUP",
							3 : "RELEASING",
							4 : "COURTESY LANE"
						};
					var quedata = {
						banner_num 			: counter_number,
						banner_name 		: "<div><status-font style='font-size: 75px;'>"+prefix[RecordDetailParsedFromForm.status] + "</status-font></div> <div style='margin-top: 0px;'><quecard-font style='font-size: 130px;'>"+result[0].quecard+"</quecard-font></div>",
						date_of_transaction : result[0].date_of_transaction
				    };
					connection.query("UPDATE queserve SET ? WHERE counter_number= ?", [RecordDetailParsedFromForm, counter_number] , function(err, rows){
						if(err){
							console.log('ERROR on que.js callclient : %s', err);
						}else{
							connection.query("UPDATE "+current_table+" SET time_called = CURRENT_TIMESTAMP() WHERE id = ?", [id], function(clrTable_err, clrTable_result){
					  			if (clrTable_err) {console.log("ERROR on que.js callclient update current_table : %s ",clrTable_err );}
							});
							connection.query("UPDATE quedata SET ? WHERE id=1",  [quedata], function(clrData_err, clrData_result){
								if (clrData_err) {console.log("ERROR on que.js callclient update quedata : %s ",clrData_err );}
							});
					  		if(err){
					  			console.log("ERROR on que.js callclient update queserve : %s ",err );
						     	var response = {success : 'ERROR' , name : req.session.name};
						     	res.send(response);
						    }else{
						     	console.log('called! '+ current_table +" "+ id);
						     	var response = {success : 'Update successful', counter : counter_number , name : req.session.name};
						     	res.send(response);
						    }
						}
				  	});
				}else{
					console.log("NO result!");
				}
			}
		 });
	 }else{
	 	console.log('counter_number is undefined. something wrong with the session %s', util.inspect(req.session, {showHidden: false, depth: null}));
	 }
};

module.exports.getrelease = function(req, res){
	var prepared = {};
	var unprepared = {};
	var released = {};
	var tobecalled = {};

	connection.query("SELECT parent_table, id, numberque, salutation, firstname, lastname, status, ar, prepared, flag, date_of_transaction FROM ( SELECT 'pending' parent_table,id, DATE_FORMAT(date_of_transaction,'%h:%i:%s %p') TIMEONLY, numberque, salutation, firstname, lastname, status, ar, prepared, flag, date_of_transaction FROM pending WHERE DATE(date_of_transaction)= CURDATE() AND prepared='0' 	UNION ALL	SELECT 'skipped' parent_table, id, DATE_FORMAT(date_of_transaction,'%h:%i:%s %p') TIMEONLY,numberque, salutation, firstname, lastname, status, ar, prepared, flag, date_of_transaction	FROM skipped WHERE DATE(date_of_transaction)= CURDATE() AND prepared='0' 	)subquery	ORDER BY numberque, FIELD(parent_table, 'pending', 'skipped') ", function(err, result_unprepared){
		if(err){
			console.log('ERROR on get release: %s', err);
		}else{
			if(result_unprepared.length){unprepared = result_unprepared;}

			connection.query("SELECT parent_table, id, numberque, salutation, firstname, lastname, status, ar, prepared, flag, date_of_transaction FROM ( SELECT 'pending' parent_table, id, DATE_FORMAT(date_of_transaction,'%h:%i:%s %p') TIMEONLY,numberque, salutation, firstname, lastname, status, ar, prepared, flag, date_of_transaction FROM pending WHERE DATE(date_of_transaction)= CURDATE() AND prepared='1' 	UNION ALL	SELECT 'skipped' parent_table, id, DATE_FORMAT(date_of_transaction,'%h:%i:%s %p') TIMEONLY,numberque, salutation, firstname, lastname, status, ar, prepared, flag, date_of_transaction	FROM skipped WHERE DATE(date_of_transaction)= CURDATE() AND prepared='1' 	)subquery	ORDER BY numberque, FIELD(parent_table, 'pending', 'skipped') ", function(error, result_prepared){
				if(error){
					console.log('ERROR on get release: %s', error);
				}else{
					if(result_prepared.length){prepared = result_prepared;}
					connection.query("SELECT parent_table, id, numberque, salutation, firstname, lastname, status, ar, prepared, flag, date_of_transaction FROM ( SELECT 'pending' parent_table, id,DATE_FORMAT(date_of_transaction,'%h:%i:%s %p') TIMEONLY,numberque, salutation, firstname, lastname, status, ar, prepared, flag, date_of_transaction FROM pending WHERE DATE(date_of_transaction)= CURDATE() AND prepared='0' 	UNION ALL	SELECT 'skipped' parent_table, id, DATE_FORMAT(date_of_transaction,'%h:%i:%s %p') TIMEONLY,numberque, salutation, firstname, lastname, status, ar, prepared, flag, date_of_transaction	FROM skipped WHERE DATE(date_of_transaction)= CURDATE() AND prepared='0' 	)subquery	ORDER BY numberque DESC, FIELD(parent_table, 'pending', 'skipped')", function(errors, result_tobecalled){
						if(errors){
							console.log('ERROR on get release: %s', errors);
						}else{
							if(result_tobecalled.length){tobecalled = result_tobecalled;}

							connection.query("SELECT id, DATE_FORMAT(time_served,'%h:%i:%s %p') TIMEONLY,numberque, salutation, firstname, lastname, status, ar, agent, date_of_transaction FROM served WHERE status = 3", function(released_errs, result_released){
								if(released_errs){
									console.log('ERROR on get release: %s', released_errs);
								}else{
									if(result_released.length){released = result_released;}
									var response = {'success': true, 'prepared' : prepared, 'unprepared' : unprepared, 'released' : result_released, 'tobecalled' : tobecalled};
									res.send(response);
								}
							});
						}
					});
				}
			});
		}
	});

};



module.exports.preparependingar = function (req , res){
	var id = req.params.id;
  	connection.query("UPDATE pending SET prepared='1' WHERE id= ?", [id] , function(err, rows){
  		if(err){
  			console.log("Error on que.js preparependingar : %s ",err );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
	     }else{
	     	var response = {success : 'Update successful' , name : req.session.name};
	     	res.send(response);
	     }
  	});
};

module.exports.prepareskippedar = function (req , res){
	var id = req.params.id;
  	connection.query("UPDATE skipped SET prepared='1' WHERE id= ?", [id] , function(err, rows){
  		if(err){
  			console.log("Error on que.js prepareskippedar : %s ",err );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
	     }else{
	     	var response = {success : 'Update successful' , name : req.session.name};
	     	res.send(response);
	     }
  	});
};

module.exports.inputque = function(req, res){
	var data = JSON.parse(JSON.stringify(req.body));
	var prepared = '';
	var alias = '';
	if(data.alias == '' || data.alias == undefined){
		alias = data.firstname.toUpperCase() +' '+ data.lastname.toUpperCase();
	}else{
		alias = data.alias.toUpperCase();
	}

	if(data.ar != 'N/A'){prepared = '0';}else{prepared = '2';}

	connection.query("SELECT * FROM number WHERE DATE(date_of_transaction) != CURDATE()", function(err, result){
		if(result.length){
			connection.query("TRUNCATE TABLE number", function(error, results){
				if(error){console.log('ERROR on que.js inputque : %s', error);}
			});
			connection.query("INSERT INTO number(id,number_sequence) VALUES(1,1)", function(error, results){
				if(error){console.log('ERROR on que.js inputque : %s', error);}
			});
			connection.query("SELECT number_sequence FROM number WHERE id='1'", function(error, results){
				if(error){console.log('ERROR on que.js inputque : %s', error);}else{
					var numbersequence = results[0].number_sequence;

					var RecordDetailParsedFromForm = {
				        numberque 	: numbersequence,
				        quecard		: data.quecard,
				        salutation 	: data.salutation,
				        firstname 	: data.firstname.toUpperCase(),
				        lastname 	: data.lastname.toUpperCase(),
				        status 		: data.status,
				        ar 			: data.ar,
				        prepared	: prepared,
				        alias		: alias
				    };

					connection.query('INSERT INTO pending set ?', RecordDetailParsedFromForm,function(insert_error, insert_res){
						if(insert_error){
				  			console.log("ERROR on que.js inputque : %s",insert_error );
					     	var response = {success : 'ERROR' , name : req.session.name};
					     	res.send(response);
					     }else{
					     	addcount();
					     	var response = {success : 'Update successful' , name : req.session.name};
					     	res.send(response);
					     }
					});
				}
			});
		}else{
			connection.query("SELECT number_sequence FROM number WHERE id='1'", function(error, results){
				var numbersequence = results[0].number_sequence + 1;

				var RecordDetailParsedFromForm = {
			        numberque 	: numbersequence,
			        quecard		: data.quecard,
			        salutation 	: data.salutation,
			        firstname 	: data.firstname.toUpperCase(),
			        lastname 	: data.lastname.toUpperCase(),
			        status 		: data.status,
			        ar 			: data.ar,
			        prepared	: prepared,
			        alias		: alias
			    };

				connection.query("UPDATE number SET number_sequence = "+ numbersequence +"  WHERE id='1'", function(errors, updateResult){
					console.log('ERROR on que.js inputque : %s', errors);
				});
				connection.query('INSERT INTO pending set ?', RecordDetailParsedFromForm, function(insert_error, insert_res){
			  		if(insert_error){
			  			console.log("ERROR on que.js inputque : %s",insert_error );
				     	var response = {success : 'ERROR' , name : req.session.name};
				     	res.send(response);
					}else{
						addcount();
						var response = {success : 'Update successful' , name : req.session.name, numberque: RecordDetailParsedFromForm.numberque};
					   
					   const ThermalPrinter = require("node-thermal-printer").printer;
					   const Types = require("node-thermal-printer").types;
					   date_of_print = new Date().toString().slice(0,24);

					   console.log(date_of_print);

					   async function example () {
					   let printer = new ThermalPrinter({
						   type: Types.EPSON,  // 'star' or 'epson'
						   interface: 'tcp://192.168.1.13:9100',
						   options: {
						   // timeout: 1000
						   },
						   width: 48,                         // Number of characters in one line - default: 48
						   characterSet: 'SLOVENIA',          // Character set - default: SLOVENIA
						   removeSpecialCharacters: false,    // Removes special characters - default: false
						   lineCharacter: "-",                // Use custom character for drawing lines - default: -
					   });

					   let isConnected = await printer.isPrinterConnected();
					   console.log("Printer connected:", isConnected);

				   
					   printer.alignCenter();
					   await printer.printImage('./node_modules/node-thermal-printer/assets/mbc.png')

					   printer.alignCenter();
					   printer.setTypeFontA();
					   printer.setTextSize(0,0);
					   printer.println("Your queue number is:");

					   printer.newLine();
					   printer.setTypeFontA();
					   printer.setTextSize(3,3);
					   printer.bold(true); 
					   printer.println(data.quecard);
					   printer.setTextNormal();
					   printer.setTextSize(0,0);
					   printer.setTypeFontB();
					   printer.println(date_of_print);
					   printer.newLine();   

					   printer.alignCenter(); 
					   printer.setTextNormal();
					   printer.setTextSize(0,0);
					   printer.setTypeFontB();
					   printer.println("Please wait for your number to be called.");

					   printer.setTypeFontA();
					   printer.setTextSize(1,1);
					   printer.bold(true); 
					   printer.println("Thank you!");     
					   printer.newLine();
					   printer.openCashDrawer();
					   printer.newLine();

					   printer.newLine();
						
						try {
							await printer.execute();
							console.log("Print success.");
						} catch (error) {
							console.error("Print error:", error);
						}
						}
						example();
				     	res.send(response);
				     }
				});
			});
		}
	});
};


module.exports.preparequetv = function(req, res){
	var counter = {};
	var called_client = {};

	connection.query("SELECT * FROM queserve q LEFT JOIN status s ON q.servicing = s.id ORDER BY q.counter_number ASC", function(counter_error, counter_result){
		if(counter_error){console.log('ERROR on que.js preparequetv : %s', counter_error);}else{
			if(counter_result.length){ counter = counter_result;}
			connection.query("UPDATE queserve SET client_name = NULL, status = NULL, numberque = NULL WHERE DATE(date_of_transaction) = CURDATE()-1 " , function(update_err, update_res){
				if(update_err){ console.log('ERROR on que.js preparequetv : %s', update_err); }else{
					connection.query('UPDATE quedata SET banner_name = NULL, banner_num = NULL WHERE DATE(date_of_transaction) = CURDATE()-1 ', function(error, results){
						if(error){ console.log('ERROR on que.js preparequetv : %s', error); }
					});
					connection.query("SELECT * FROM quedata", function(called_error, called_result){
						if(called_error){
							console.log('ERROR on que.js preparequetv : %s', called_error);
						}else{
							if(called_result.length){ called_client = called_result;}
							var response = {'counter': counter, 'called_client' : called_client};
							res.send(response);
						}
					});
				}
			});
		}
	});
};


module.exports.getcounters = function(req, res){
	connection.query('SELECT * FROM queserve', function(err, results){
		if(err){
  			console.log("Error on que.js getcounters : %s ",error );
	     	var response = {success : 'ERROR' , name : req.session.name};
	     	res.send(response);
	     }else{
	     	var response = {success : results , name : req.session.name};
		    res.send(response);
	     }
	});
};


module.exports.setSafe = function(req, res){
    connection.query('SET SQL_SAFE_UPDATES = 1', function(error, result){
      if(error){
        console.log('ERROR on serSafe : %s', error);
      }else{
        console.log('safe');
        var response = {message : 'setsafe'};
        res.send(response);
      }
    });
};