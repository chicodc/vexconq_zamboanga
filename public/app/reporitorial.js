
function formatDate(date) {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + '-' + minutes + '' + ampm;
  return date.getFullYear() +"-"+ (date.getMonth() + 1) + "-" + date.getDate() + "_" + strTime;
}

var app = angular.module('Report', ['ngAnimate','ui.bootstrap', 'smart-table-improved']);

app.controller('ReportController',['$scope','$rootScope','$route', '$compile', '$uibModal','$log', '$routeParams', '$location', 'generalFactory', 'socket', '$timeout',
	function($scope, $rootScope, $route, $compile, $uibModal, $log, $routeParams, $location, generalFactory, socket, $timeout){
	
	
	function session_check(){
		generalFactory.getHandler('/login/check', function(response){
			if(response.loggedin == true && response.type == 'Admin'){
				$location.path('/queuing');
			}else{
				console.log(response.loggedin);
				$location.path('/');
			}
		});	
	};	
	
	$scope.back = function(){
		$location.path('/menu');
	};
	
	$scope.logout = function(){
		generalFactory.getHandler('/logout', function(response){
			if(response.destroyed){
				 $location.path('/');
			}
		});	
	};
	
	$scope.open = function (size, url, report) {
		
			if($scope.dateFrom && $scope.dateTo){
				if(report != 'table'){
					$scope.chart = report;
	   				$scope.modal(size, url);
				}else{
					generalFactory.postHandler('/reporitorial/getdata', {dateFrom : $scope.dateFrom, dateTo : $scope.dateTo},  function(response){
						if(response.success != 'ERROR'){
							$scope.data = response.success;
							$scope.modal(size, url);
						}
					});
				}	
			}else{
				alert('Please input the dates first. Thank you!');
			}
	};
	
	$scope.openPerYear = function (size, url, report) {
			if($scope.yearmonth){
				$scope.chart = report;
	   			$scope.modal(size, url);
	   		}else{
	   			alert('Please input the dates first. Thank you!');
	   		}
	};
	
	$scope.modal = function(size, url){
		var modalInstance = $uibModal.open({
									      	animation: true,
									      	templateUrl: url,
									      	controller: 'ReportModal',
									      	size: size,
									      	scope: $scope,
									      	windowClass: 'app-modal-window',
									      	resolve: {
									        		items: function () {
									          		return $scope;
									        	}
									     	}
	    					});
	
	    modalInstance.result.then(function (selectedItem) {
	      	$scope.selected = selectedItem;
	    }, function () {
	      	$log.info('Modal dismissed at: ' + new Date());
	    });
	};

}]);

app.controller('ReportModal' , ['$timeout', 'items', '$scope', '$route', '$uibModalInstance', '$location', 'generalFactory', 
	function ($timeout , items, $scope, $route, $uibModalInstance, $location, generalFactory ) {
	
	$scope.SAVE_REPORT = $scope.$parent.SAVE_REPORT;
	$scope.PRINT_REPORT = $scope.$parent.PRINT_REPORT;
	
	$scope.tableData = items.data;
	
	$scope.itemsByPage=15;
	$scope.summaryPercentage = false;
	$scope.summaryPerDate = false;
	$scope.summaryPerWeekDay = false;
	
	$scope.update = function() {
	   
	   if($scope.selectedItem == 'all'){
	   	$scope.itemsByPage = items.data.length;
	   }else{
	   	$scope.itemsByPage = $scope.selectedItem;
	   }
	   $timeout(function(){
        	$scope.$apply();
	   		console.log($scope.itemsByPage);
        },100);
	};
	
	$scope.cancel = function () {
	   	$uibModalInstance.dismiss('cancel');
	};
	
	$scope.exportToExcel=function(tableId){ 
        $timeout(function(){
        	$scope.$broadcast('doneloadingtable', { from: items.dateFrom, to: items.dateTo, elementid : tableId});
        },100); 
  	};
        
  	$scope.print = function(divid){
  		$timeout(function(){
    		$('.modal').css('position', 'static');
        	$scope.$broadcast('doneloadingdiv', { from: items.dateFrom, to: items.dateTo, elementid : divid});
        },100); 
  	};
  	
  	$scope.printall = function(){
		
		$("#modalbody").printThis({ 
		    debug: false,              
		    importCSS: true,             
		    importStyle: true,         
		    printContainer: true,       
		    loadCSS: ["http://10.0.1.2:8080/bootstrap/dist/css/bootstrap.min.css", 
		    		  "http://10.0.1.2:8080/stylesheets/quelogin.css", 
		    		  "http://10.0.1.2:8080/stylesheets/sbadmintheme/css/style-override.css"], 
		    pageTitle: "My Modal",             
		    removeInline: false,        
		    printDelay: 333,            
		    header: null,             
		    formValues: true          
		});
  	};
  	
  	$scope.opencharts = function(){
  		var url = '';
  		
  		generalFactory.postHandler('/reporitorial/perservice', {yearmonth : items.yearmonth, from : items.dateFrom, to : items.dateTo , charttype : items.chart},function(response){
  				
  				if(response.success === 'ERROR'){
  					$scope.showTable = false;
  					$scope.noData = response.message;
  				}else{
  					if(items.chart === 'percentage' || items.chart === 'clientPerDate' || items.chart === 'clientPerWeekDay' || items.chart === 'timePerAgent'){
  						$scope.loadSummary();
  					}
  					
  					$timeout(function(){
						$scope.datatable = response.success;
 						$scope.$broadcast('chartDataLoaded', { yearmonth : items.yearmonth, from : items.dateFrom, to : items.dateTo , chart : items.chart, result : response.success, exporting: $scope.SAVE_REPORT });
						
  						$scope.getServices();

						console.log($scope.datatable);	
  					},100);
  				}
  		});
  	};
  	
  	$scope.getServices = function(){
  		generalFactory.getHandler('/main/getServices', function(res){
			if(res.status === "SUCCESS"){
				$scope.services = res.result;
			}
		});
  	};

  	$scope.loadSummary = function(){
  		generalFactory.postHandler('/reporitorial/summary', {from : items.dateFrom, to : items.dateTo, charttype : items.chart }, function(response){
  			if(response.success != 'ERROR'){	
  				$scope.summaryTable = response.success;
  			}
  		});
  	};
  	
}]);

app.directive('dateelem', ['$timeout', function ($timeout) {
  return {
    link: function ($scope, element, attrs) {
    	$(".fromthisyearpicker").datepicker({ dateFormat: 'yy-mm-dd' , maxDate : '0'});
 		$(".tothisyear").datepicker({ dateFormat: 'yy-mm-dd' , maxDate : '0'});
 		$(".yearmonth").datepicker( {changeMonth: true, changeYear: true, dateFormat: 'mm:dd:yy',maxDate : '0'});
    }
  };
}]);

app.directive('pageSelect', [function() {
  return {
    restrict: 'E',
    template: '<input type="text" class="select-page" ng-model="inputPage" ng-change="selectPage(inputPage)">',
    link: function(scope, element, attrs) {
      scope.$watch('currentPage', function(c) {
        scope.inputPage = c;
      });
    }
  };
}]);

app.directive('divreport', ['$timeout', function ($timeout) {
  return {
    link: function ($scope, element, attrs) {
      $scope.$on('doneloadingtable', function (event, args) {
        $timeout(function () { 
            	 var blob = new Blob([$(args.elementid).html()], {
		            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
		        });
		        var date = new Date();
				var datetime = formatDate(date);
		        saveAs(blob, "Report-"+datetime+".xls");
        	
        }, 0, false);
      });
      $scope.$on('doneloadingdiv', function (event, args) {
      	console.log(args.elementid);
        $timeout(function () { 
				var month = ["", "JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"];
				var data = Array();
				    
				$(args.elementid + " tr").each(function(i, v){
				    data[i] = Array();
				    $(this).children().each(function(ii, vv){
				        data[i][ii] = $(this).text();
				    }); 
				});
				var from = new Date(args.from);
				var to = new Date(args.to);
				
				var newFrom = month[parseInt(from.getMonth())] +' '+from.getDate()+', '+from.getFullYear();
				var newTo = month[parseInt(to.getMonth())] +' '+to.getDate()+', '+to.getFullYear();
				
		        var docDefinition = {
				 	pageSize: 'LETTER',
	  				pageOrientation: 'landscape',
	  				//watermark:'a very very very lonne testing watermmark',
		            content: [
		            		{ text: 'Daily Transactions', style: 'header' , alignment: 'center'},
							{ text: 'FROM :' + newFrom + ' -  TO : '+ newTo, style: 'subheader' , alignment: 'center'},
		            		{
							style: 'tableExample',
							table: {	
									headerRows: 1,
									body: data
							},
							layout: {
							hLineWidth: function(i, node) {
									return (i === 0 || i === node.table.body.length) ? 2 : 1;
							},
							vLineWidth: function(i, node) {
									return (i === 0 || i === node.table.widths.length) ? 2 : 1;
							},
							hLineColor: function(i, node) {
									return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
							},
							vLineColor: function(i, node) {
									return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
							},
							// paddingLeft: function(i, node) { return 4; },
							// paddingRight: function(i, node) { return 4; },
							// paddingTop: function(i, node) { return 2; },
							// paddingBottom: function(i, node) { return 2; }
						}
						}],
					styles: {
						header: {
							fontSize: 18,
							bold: true,
							margin: [0, 0, 0, 0]
						},
						subheader: {
							fontSize: 13,
							bold: true,
							margin: [0, 10, 0, 5]
						},
						tableExample: {
							fontSize: 10,
							margin: [0, 5, 0, 15]
						},
						tableHeader: {
							bold: true,
							fontSize: 13,
							bold: true,
							color: 'black'
						}
					},
				};
			
        	 pdfMake.createPdf(docDefinition).open();
        	 
        	$('.modal').css('position', 'fixed');
        	
        }, 0, false);
      });
      
      
      /*TRY ONLY*/
     $scope.$on('doneloadingdivTry', function (event, args) {
        $timeout(function () { 
		        var docDefinition = {
				 	pageSize: 'LETTER',
	  				pageOrientation: 'landscape',
		            content: [
		            		{
		            		image: args.image,
		            		width: 1000,
							pageBreak: 'after'
		            		},
		            		{
							style: 'tableExample',
							table: {
									widths: ['*', 'auto'],
									body: args.elementid
							},
							layout: {
							hLineWidth: function(i, node) {
									return (i === 0 || i === node.table.body.length) ? 2 : 1;
							},
							vLineWidth: function(i, node) {
									return (i === 0 || i === node.table.widths.length) ? 2 : 1;
							},
							hLineColor: function(i, node) {
									return (i === 0 || i === node.table.body.length) ? 'black' : 'gray';
							},
							vLineColor: function(i, node) {
									return (i === 0 || i === node.table.widths.length) ? 'black' : 'gray';
							}
						}
						}],
					styles: {
						header: {
							fontSize: 18,
							bold: true,
							margin: [0, 0, 0, 0]
						},
						subheader: {
							fontSize: 13,
							bold: true,
							margin: [0, 10, 0, 5]
						},
						tableExample: {
							fontSize: 10,
							alignment:"center",
							margin: [0, 5, 0, 15]
						},
						tableHeader: {
							fillColor:"#06C",
							alignment:"center",
							bold: true,
							fontSize: 13,
							bold: true,
							color: 'white'
						}
					},
				};
			
        	 pdfMake.createPdf(docDefinition).open();
        	 
        	$('.modal').css('position', 'fixed');
        	
        }, 0, false);
      });
      /*END TRY ONLY*/
      
      $scope.$on('chartDataLoaded', function (event, args) {
      	$scope.showTable = true;
      	var options = {};
      	var month = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"];
	    
	    
      	var data = [];
		for (var i = 0; i < args.result.length; ++i) {
		    	var obj = args.result[i];
    			data[obj.status] = $.map(obj, function(value, index) {
				    if(index != 'status' && index != 'year' && index != '$$hashKey'){
				    		return [value];
				    }
				});	
		}
      	switch(args.chart){
      		case 'percentage':
      			var data = [];
      			var count = 0;
      			args.result.forEach(function(item, index){
      				data[count] = {
      					name: item.category,
      					y: parseInt(item.PERCENT)
      				};
      				count++;
      			});
      			
      			$scope.showTable = false;
				$scope.summaryPercentage = true;
				
	      		options.chart = {
				                	type: 'pie',
				                	options3d: {
								                enabled: true,
								                alpha: 45,
								                beta: 0
								            }
				           		};
      			options.title = {text : 'Percentage of service : ' + args.from + ' TO ' + args.to};
      			options.subtitle = {text : ''};
      			options.xAxis = {type : 'category'};
      			options.yAxis = {title : {text : 'Total percent'} };
      			options.plotOptions = {	
					      				pie: {
					                    	allowPointSelect: true,
					                    	cursor: 'pointer',
					                    	depth: 35,
					                    	dataLabels: {
										                    enabled: true,
										                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
										                    style: {
										                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
										                    }
										                },
					                    	showInLegend: true
					                	}
					                };
      			options.tooltip =  {
				            		pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
				        		};
				options.series =  [{
									name: 'SERVICE',
						            colorByPoint: true,
						            data: data
						        }];
				options.credits = {enabled : false};
		      		
      		break;
      		
      		case 'averageServiceTime' : 
      			var title = 'Average service time(in minutes) for :' + args.from + " TO " + args.to;
      				options.data = {table: 'datatable'};
			        options.chart = {type: 'spline'};
			        options.title = {text: title};
			        options.yAxis = {
							           	allowDecimals: true,
							            title: {
							                text: 'MINUTES'
							            }
							        };
			        options.plotOptions = {
								            column: {
								                dataLabels: {
								                    enabled: true,
								                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray'
						                		}
								            }
								        };
					options.exporting = { enabled: args.exporting	};
			        options.tooltip = {
							            formatter: function () {
							                return '<b>' + this.series.name + '</b><br/>' +
							                    this.point.y + ' mins'; // + this.point.name.toLowerCase();
							            }
							        };
					options.credits = {enabled : false};
      		break;
      		
      		case 'timePerAgent' : 
	      	 	
      			$scope.showTable = false;
				$scope.summaryPerDate = true;
				$scope.category = 'AGENT';
				
	      	 	var title = 'Average service time(in minutes) Per agent for :' + args.from + " TO " + args.to;
      				options.data = {table: 'datatable'};
			        options.chart = {type: 'column'};
			        options.title = {text: title};
			        options.yAxis = {
							           	allowDecimals: true,
							            title: {
							                text: 'MINUTES'
							            },
							            stackLabels: {
							                enabled: true,
							                style: {
							                    fontWeight: 'bold',
							                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
							                }
							            }
							        };
			        options.plotOptions = {
							            column: {
							            	stacking : 'normal',
							                dataLabels: {
							                    enabled: false,
							                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray'
					                		}
							            }
							        };
					options.exporting = { enabled: args.exporting	};
			        options.tooltip = {
							            formatter: function () {
							                return '<b>' + this.series.name + '</b><br/>' +
							                    this.point.y + ' mins'; // + this.point.name.toLowerCase();
							            }
							        };
					options.credits = {enabled : false};
      		
      		break;
      		
      		case 'clientPerDate' :
      			
      			$scope.showTable = false;
      			$scope.summaryPerDate = true;
      			$scope.category = "DATE";
      			
      			var data = [];
				for (var i = 0; i < args.result.length; ++i) {
				    	var obj = args.result[i];
		    			data[i] = $.map(obj, function(value, index) {
						    if(index != 'status' && index != 'year' && index != '$$hashKey'){
						    		return [value];
						    }
						});	
				}
		      	
      			var title = args.from + " TO " + args.to;
		   		
				     options.chart = { type: 'column' };
				     options.title = { text: 'Client Count for ' + title };
				     options.xAxis = {
							            type: 'category',
							            labels: {
							                rotation: -45,
							                style: {
							                    fontSize: '13px',
							                    fontFamily: 'Verdana, sans-serif'
							                }
							            }
							       };
				     options.yAxis = {
				            			min: 0,
				            			title: {
				                			text: 'Client Count'
				            			}
				       				};
				     options.legend = { enabled: false };
				     options.tooltip = { pointFormat: 'Client count: <b>{point.y:,.0f}</b>' },
				     options.exporting = { enabled: args.exporting };
				     options.series = [{
								            name: 'Client Count',
								            data: data,
								            dataLabels: {
								                enabled: true,
						                    	color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray',
								                align: 'right',
								                format: '{point.y:,.0f}'
								            }
								        }];
					options.credits = {enabled : false};
      			
      		break;
      		
      		case 'clientPerHour' : 
      			var title = 'Client Arrival PER HOUR : ' + args.from + " TO " + args.to;
      			
      			options.data = { table: 'datatable' };
		        options.chart = { type: 'column' };
		       	options.title = { text: title };
		        options.yAxis = {
						            allowDecimals: true,
						            title: {
						                text: 'Client Count'
						            },
						            stackLabels: {
						                enabled: true,
						                style: {
						                    fontWeight: 'bold',
						                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
						                }
						            }
						        };
		        options.plotOptions = {
							            column: {
							            	stacking : 'normal',
							                dataLabels: {
							                    enabled: false,
							                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray'
					                		}
							            }
							        };
		        options.exporting =  { enabled: args.exporting };
		        options.tooltip = { formatter: function () { return '<b>' + this.series.name + '</b><br/>' + this.point.y; } };
				options.credits = {enabled : false};

      			
      		break;
      		
      		case 'clientPerWeekDay' : 
      		
      			$scope.showTable = false;
				$scope.summaryPerWeekDay = true;
				
      	 		var	title = 'Client Arrival PER WEEKDAY : ' + args.from + " TO " + args.to;
      	 		
      			options.data = { table: 'datatable' };
		        options.chart = { type: 'column' };
		       	options.title = { text: title };
		        options.yAxis = {
						            allowDecimals: true,
						            title: {
						                text: 'Client Count'
						            },
						            stackLabels: {
						                enabled: true,
						                style: {
						                    fontWeight: 'bold',
						                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
						                }
						            }
						        };
		        options.plotOptions = {
							            column: {
							            	stacking : 'normal',
							                dataLabels: {
							                    enabled: false,
							                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray'
					                		}
							            }
							        };
		        options.exporting =  { enabled: args.exporting };
		        options.tooltip = { formatter: function () { return '<b>' + this.series.name + '</b><br/>' + this.point.y; } };
				options.credits = {enabled : false};
      			
      			
      		break;
      		
      		case 'monthlyPercentage' :
      				var dataSort = [];
	      			var monthlyData = [];

					for (var i = 0; i < args.result.length; ++i) {
					    	var obj = args.result[i];
			    			monthlyData = $.map(obj, function(value, index) {
							    if(index != 'status' && index != 'year' && index != '$$hashKey'){
							    		return [parseInt(value)];
							    }
							});	

							dataSort[i] = {
		      					name: args.result[i].status,
		      					data: monthlyData
		      				};
					}

      				$scope.showTable = false;
					options.chart = {type: 'column'};
					options.title = {text: 'PERCENTAGE PER SERVICE'};
					options.subtitle = {text: 'Year: ' + args.yearmonth};
					options.xAxis = {	categories: month,
							            tickmarkPlacement: 'on',
							            title: {enabled: false}
							        };
					options.yAxis = {title: {text: 'Percent'}};
					options.tooltip = { pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b> ({point.y:,.0f} )<br/>',
            							shared: true
       								  };
		       		options.plotOptions = {column : 
		       									{ stacking: 'percent',
									               dataLabels: {
									                    enabled: true,
									                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
									                    style: { textShadow: '0 0 3px black, 0 0 3px black' }
							                		}
									            }
									       };
						
			        options.exporting =  { enabled: args.exporting };
			        options.series = dataSort;
					options.credits = {enabled : false};
      				
      		break;
      		
      		case 'monthlyAverage' : 
      			var title = 'Average service time(in minutes) for :' + args.yearmonth;
      				options.data = {table: 'datatable'};
			        options.chart = {type: 'spline'};
			        options.title = {text: title};
			        options.yAxis = {
							           	allowDecimals: true,
							            title: {
							                text: 'MINUTES'
							            }
							        };
			        options.plotOptions = {
								            column: {
								                dataLabels: {
								                    enabled: true,
								                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'gray'
						                		}
								            }
								        };
					options.exporting = { enabled: args.exporting	};
			        options.tooltip = {
							            formatter: function () {
							                return '<b>' + this.series.name + '</b><br/>' +
							                    this.point.y + ' mins'; // + this.point.name.toLowerCase();
							            }
							        };
					options.credits = {enabled : false};
      		break;
      	};
      	
      	 $timeout(function () { 
      	 	$scope.$apply();
      	 	$('#container').highcharts(options);
		 }, 0, false);
		   
      });
    }
  };
}]);
   
app.directive('yearDrop',function(){
   	function getYears(){
        var start = 2015;
        var currentYear = new Date().getFullYear();
        var years = [];
        var i = 0;
        while(start <= currentYear){
        	years.push(start);
        	start++;
        }
        return years;
    }
    return {
        link: function(scope,element,attrs){
            scope.years = getYears();
            scope.selected = scope.years[0];
        },
        template: '<select class="form-control" ng-model="yearmonth" ng-options="y for y in years"></select>'
    };
});  
     
app.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
});           
