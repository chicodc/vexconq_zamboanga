//'use strict';

var app = angular.module('Login', []);


app.controller('LoginController',['$scope', '$rootScope','$route', '$window', '$q', '$routeParams', '$location', '$timeout', '$uibModal', '$log', 'generalFactory',
	function($scope, $rootScope, $route,  $window, $q, $routeParams, $location, $timeout, $uibModal, $log, generalFactory){

		$scope.menu = "";
		$scope.account = "";


		$scope.session_check = function(){
			generalFactory.getHandler('/login/check', function(response){
				if(response.loggedin === true){
					$location.path('/queuing');
				}else{
					console.log(response.loggedin);
					$location.path('/');
				}
			});
		};

		$scope.clear = function(){
			generalFactory.getHandler('/login/clearall', function(response){
				console.log(response);
			});
		};

		$scope.reroute = function(url){
			console.log(url);
			$location.path(url);
		};

		$scope.initlogin = function(){
			console.log('initialized login');
			socket.emit('onlogin');
			var allPromises = $q.all([
				$scope.clear(),
				$scope.session_check()
			]);

			allPromises.then(function(){
				var timeout = $timeout(function(){
					$('#loading_screen').fadeOut();
					$timeout.cancel(timeout);
				}, 3000, false);

				timeout.then(function(){
					$timeout.cancel(timeout);
				}, function(){
					console.log('Timer destroyed', Date.now());
				});
			});
		};

		$scope.initmenu = function(){
			generalFactory.getHandler('/login/check', function(response){
				$scope.account = response.type;
				console.log(response.type);
					if($scope.account == 'Admin'){

					var menu1 = {	"div": "b1que",
								    "buttons": {"button1" : {
								        "name" : "butts1",
								        "class" : "buttons1",
								        "url" : "/frontline"
								    },"button2" : {
								        "name" : "butts1",
								        "class" : "buttons1a",
								        "url" : "/frontline"
								    }}
								};

					var menu2 = {	"div": 'b2que',
									"buttons": {"button1" : {
								        "name" : "rels",
								        "class" : "buttons2",
								        "url" : "/backroom"
								    },
								    "button2" : {
								        "name" : "rels",
								        "class" : "buttons2_1",
								        "url" : "/backroom"
								    }}
								};

					var menu3 = { 	"div": "b3que",
									"buttons": {"button1" : {
								        "name" : "apps",
								        "class" : "buttons3",
								        "url" : "/admin"
								    },
								    "button2" : {
								        "name" : "apps",
								        "class" : "buttons31a",
								        "url" : "/admin"
								    }}
								};
					var menu4 = {	"div": "b4que",
									"buttons": {"button1" : {
								        "name" : "inqs",
								        "class" : "buttons4",
								        "url" : "/report"
								    },
								    "button2" : {
								        "name" : "inqs",
								        "class" : "buttons4a",
								        "url" : "/report"
								    }}
								};

					$scope.menu = [ menu1 ,menu2, menu3, menu4 ];

				}else{
					var menu1 = {	"div": "b_1que",
								    "buttons": {"button1" : {
								        "name" : "butts1",
								        "class" : "buttons1s",
								        "url" : "/frontline"
								    },"button2" : {
								        "name" : "que_1a",
								        "class" : "buttons_1s",
								        "url" : "/frontline"
								    }}
								};

					var menu2 = {	"div": 'b_2que',
									"buttons" : {"button1" : {
								        "name" : "rels2",
								        "class" : "buttons2",
								        "url" : "/backroom"
								    },
								    "button2" : {
								        "name" : "rels2",
								        "class" : "buttons_2s",
								        "url" : "/backroom"
								    }}
								};
					$scope.menu = [ menu1 ,menu2 ];
				}
			});
		};

		$scope.login = function(){
			$scope.error = false;
			$scope.success = false;

			generalFactory.postHandler('/login/verify', { username: $scope.username, password: $scope.password }, function(response){
				if(!response.success){
					 $scope.error = response.message;
				} else {
					console.log(response.change);
					if(response.success === 'unchanged'){
						$scope.openmodal('', 'partials/form_changepass.html');
					}else{
						$scope.account = response.success.account_type;
						$location.path('/queuing');
					}

				}
			});

		};

		$scope.closeError = function(){
			$scope.error = false;
		};

		$scope.logout = function(){
			generalFactory.getHandler('/logout', function(response){
				if(response.destroyed){
					 $location.path('/');
				}
			});
		};

		$scope.openmodal = function (size, url) {
	    	var modalInstance = $uibModal.open({
		      	animation: true,
		      	templateUrl: url,
		      	controller: 'ChangePasswordController',
		      	size: size,
		      	backdrop: 'static',
		      	keyboard: false,
		      	scope: $scope,
		      	windowClass: 'app-modal-window',
		      	resolve: {
		        		items: function () {
		          		return $scope;
		        	}
		     	}
		    });

		    modalInstance.result.then(function (selectedItem) {
		      	$scope.selected = selectedItem;
		    }, function () {
		      	$log.info('Modal dismissed at: ' + new Date());
		    });
		};


}]);


app.controller('IO_LoginController', ['$scope', '$route', '$routeParams', '$location', '$timeout', '$uibModal', 'generalFactory',function($scope, $route, $routeParams, $location, $timeout, $uibModal, generalFactory){
	var io = $routeParams.io;

	$scope.initlogin = function(){
			console.log('initialized ' + io + ' login');
			socket.emit('onlogin');
			$scope.sessionCheck();

			var timeout = $timeout(function(){
				$('#loading_screen').fadeOut();
				$timeout.cancel(timeout);
			}, 3000, false);
		};

	$scope.openmodal = function (size, url) {
	    	var modalInstance = $uibModal.open({
		      	animation: true,
		      	templateUrl: url,
		      	controller: 'ChangePasswordController',
		      	size: size,
		      	backdrop: 'static',
		      	keyboard: false,
		      	scope: $scope,
		      	windowClass: 'app-modal-window',
		      	resolve: {
		        		items: function () {
		          		return $scope;
		        	}
		     	}
		    });

		    modalInstance.result.then(function (selectedItem) {
		      	$scope.selected = selectedItem;
		    }, function () {
		      	$log.info('Modal dismissed at: ' + new Date());
		    });
		};


		$scope.login = function(){
						$scope.error = false;
						$scope.success = false;

						generalFactory.postHandler('/login/ioVerify', { username: $scope.username, password: $scope.password }, function(response){
							if(!response.success){
								 $scope.error = response.message;
							} else {
								console.log(response.change);
								if(response.success === 'unchanged'){
									$scope.openmodal('', 'partials/form_changepass.html');
								}else{
									$scope.account = response.success.account_type;
									if(io === 'ipad'){
										$location.path('/ipadque');
									}else if(io === 'tv'){
										$location.path('/quetv');
									}else{
										$location.path('/quelist');
									}
								}

							}
						});
				};

				$scope.sessionCheck = function(){
					generalFactory.getHandler('/login/ioCheck', function(response){
							if(response.iologgedin === true){
								console.log(response.iologgedin);
								if(io === 'ipad'){
									$location.path('/ipadque');
								}else if(io === 'tv'){
									$location.path('/quetv');
								}else{
									$location.path('/quelist');
								}
							}else{
								console.log(response.iologgedin);
								$location.path('/iologin/'+io);
							}
						});
				};
}]);

app.controller('ChangePasswordController' , ['items', '$scope', '$route', '$timeout','$uibModalInstance', '$location', 'generalFactory',
	function (items, $scope, $route, $timeout, $uibModalInstance, $location, generalFactory ) {
	$scope.cancel = function(){
		$uibModalInstance.dismiss('cancel');
	};

	$scope.change = function(){
		generalFactory.postHandler('/login/changepass', { username: $scope.username, password: $scope.newpass }, function(response){
				if(!response.success){
					 $scope.error = response.message;
				} else {
					$uibModalInstance.dismiss('cancel');
					$scope.$parent.username = '';
					$scope.$parent.password = '';
					$scope.$parent.success = 'Password changed successfully. Please login with your new password.';
				}
			});
	};

}]);
