var express = require('express');
 	path = require('path'),
 	favicon = require('serve-favicon'),
 	logger = require('morgan'),
 	cookieParser = require('cookie-parser'),
 	bodyParser = require('body-parser'),
	session = require('express-session'),
	MySQLStore = require('express-mysql-session')(session);


//not working
// load jquery
//var $ = require('jquery');
// load everything
//require('jquery-ui');
//require('dialog');



//Pages Routes
var fs = require('fs');
var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));
var conn = require('./routes/conn'),
 	routes = require('./routes/index'),
 	system = require('./routes/system');
 	users = require('./routes/users'),
 	que = require('./routes/que');


var options = {
    checkExpirationInterval: 900000,// How frequently expired sessions will be cleared; milliseconds.
    expiration: 86400000,// The maximum age of a valid session; milliseconds.
    createDatabaseTable: true,// Whether or not to create the sessions database table, if one does not already exist.
    schema: {
        tableName: 'sessions',
        columnNames: {
            session_id: 'session_id',
            expires: 'expires',
            data: 'data',
            userdata: 'userdata'
        }
    }
};





var sessionStore = new MySQLStore(options, conn);
var app = express();

//make socket io work
// var http = require('http');
// var server = http.createServer(app).listen(config.PORT, config.IP_ADD);
// var io = require('socket.io').listen(server);



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, '/node_modules')));

/*app.use(session({
    key: 'session_cookie_name',
    secret: 'session_cookie_secret',
    store: sessionStore,
    resave: true,
    cookie:{maxAge:new Date(Date.now() + 3600000)},
    maxAge : new Date(Date.now() + 3600000),
    expires : new Date(Date.now() + 3600000),
    saveUninitialized: true
}));*/

app.use(session({
    key: 'session_cookie_name',
    secret: 'session_cookie_secret',
    store: sessionStore,
    resave: false,
    duration: 30 * 60 * 1000,
  	activeDuration: 5 * 60 * 1000,
    saveUninitialized: true
}));


//HTTP calls
//login
app.get('/login/clearall', system.clearall);
app.post('/login/ioVerify', system.verify);
app.get('/login/ioCheck', system.check);

app.get('/login/check', users.sessioncheck);
app.post('/login/verify', users.verify);
app.post('/login/changepass', users.changepass);
app.get('/logout', users.logout);
app.get('/logout/disconnect', users.disconnectUser);

//main
app.get('/frontline/handle', users.handle);
app.get('/frontline/getmenu', users.getusermenu);
app.get('/frontline/getpermission', users.getuserpermission);
app.get('/frontline/getcounter', que.getcounters);
app.get('/main/getServices', system.getServices);

//frontline
app.get('/frontline/updateCounter', users.updateQue);
app.post('/frontline/changeCounter', users.changeHandle);
app.get('/frontline/vacateCounter', users.clearHandle);
app.get('/frontline/myCounter', users.getMyCounter);
app.get('/frontline/pending/:all', que.getallpending);
app.get('/frontline/skipped', que.getallskipped);
app.get('/frontline/served', que.getallserved);
app.get('/frontline/current', que.getcurrent);
app.get('/frontline/currentskipped', que.getcurrentskipped);
app.get('/frontline/next', que.nextclient);
app.get('/frontline/serve/:jumpnumber', que.serveclient);
app.get('/frontline/skip', que.skipclient);
app.post('/frontline/jump', que.jumpclient);
app.get('/frontline/breaktime', users.togglebreak);
app.get('/frontline/call', que.callclient);
app.get('/frontline/setSafe', que.setSafe);

//backroom
app.get('/backroom/forrelease', que.getrelease);
app.get('/backroom/prepareskipped/:id', que.prepareskippedar);
app.get('/backroom/preparepending/:id', que.preparependingar);

//ipadque
app.post('/ipadque/service', que.inputque);

//quetv
app.get('/quetv/initialize', que.preparequetv);
app.get('/quetv/getpending/:all', que.getallpending);

//admin
app.get('/admin/settings', users.getsettings);
app.get('/admin/permissions', users.getpermissions);
app.get('/admin/menus', users.getmenus);
app.post('/admin/add', users.adduser);
app.post('/admin/activate', users.activate);
app.post('/admin/promote', users.promote);
app.get('/admin/delete/:id', users.deleteuser);
app.post('/admin/permission', users.setpermissions);
app.post('/admin/menu', users.setmenus);
app.get('/admin/levels', users.getlevels);
app.post('/admin/level', users.setLevel);
app.post('/admin/reset', users.resetpass);
app.post('/admin/logoutUser', users.logoutUser);

//reporitorial
app.post('/reporitorial/getdata',que.getallservedarchive);
app.post('/reporitorial/perservice', que.getperservice);
app.post('/reporitorial/summary', que.getSummary);


app.use('/*', routes);
var util = require('util');

//app.use('/', routes);
//app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
   	res.render('index');
    res.render('error', {
      message: err.message,
      error: err
    });
  console.log(err);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('index');
  res.render('error', {
    message: err.message,
    error: err.status
  });
  console.log(err);
});


module.exports = app;
